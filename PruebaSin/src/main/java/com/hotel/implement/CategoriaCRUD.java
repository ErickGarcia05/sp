/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotel.implement;
import com.hotel.dao.AbsFacade;
import com.hotel.dao.Dao;
import com.hotel.models.Categoria;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
/**
 *
 * @author christian.ramirezusa
 */
@Stateless
public class CategoriaCRUD extends AbsFacade<Categoria> implements Dao<Categoria>{
    EntityManager em;
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    public CategoriaCRUD() {  
        super(Categoria.class);
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("pruebaPU");
        em = emf.createEntityManager();
    }
    /*
    public List<Categoria> otraList(){ 
        
        try {
            //Query query = em.createNamedQuery("categoria");
            List<Categoria> list = 
            return list;
        } catch (Exception e){
            System.out.println("Error al hacer consulta");
            return null;
        }
        
    }
*/
}