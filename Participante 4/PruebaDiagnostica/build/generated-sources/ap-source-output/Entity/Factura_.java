package Entity;

import Entity.Cliente;
import Entity.ModoPago;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2020-01-23T09:13:35")
@StaticMetamodel(Factura.class)
public class Factura_ { 

    public static volatile SingularAttribute<Factura, Integer> id_factura;
    public static volatile SingularAttribute<Factura, Date> fecha;
    public static volatile SingularAttribute<Factura, Cliente> id_cliente;
    public static volatile SingularAttribute<Factura, ModoPago> num_pago;
    public static volatile SingularAttribute<Factura, String> num_factura;

}