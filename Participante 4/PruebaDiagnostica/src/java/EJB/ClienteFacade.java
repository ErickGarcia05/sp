/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import Entity.Cliente;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author christian.ramirezusa
 */
@Stateless
public class ClienteFacade extends AbstractFacade<Cliente> implements ClienteFacadeLocal {

    @PersistenceContext(unitName = "PruebaDiagnosticaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ClienteFacade() {
        super(Cliente.class);
    }
    
    @Override
    public void call(String numFactura, String fecha, int c, int pago){
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("sp_insercion_factura");
            sp.setParameter(1, numFactura);
            sp.setParameter(2, fecha);
            sp.setParameter(3, c);
            sp.setParameter(4, pago);
            
            if(sp.execute()){
                System.out.println("Se ejecuto el SP");
            }
            else{
                System.out.println("Error al ejecutar el SP");
            }
            
        } catch (Exception e) {
            System.out.println("Error del metodo Cliente Facade");
        }
    }
    
    @Override
    public List<Cliente> call(){
        try {
            StoredProcedureQuery sp = em.createStoredProcedureQuery("sp_MostrarCliente");
            sp.execute();
            List<Cliente> lista = sp.getResultList();
            
            if(sp.execute()){
                System.out.println("Se ejecuto el SP");
            }
            else{
                System.out.println("Error al ejecutar el SP");
            }
            return lista;
            
        } catch (Exception e) {
            System.out.println("Error del metodo Cliente Facade");
            return null;
        }
    }
    
}
