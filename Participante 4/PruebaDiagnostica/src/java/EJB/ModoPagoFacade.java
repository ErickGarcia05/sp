/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import Entity.ModoPago;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author christian.ramirezusa
 */
@Stateless
public class ModoPagoFacade extends AbstractFacade<ModoPago> implements ModoPagoFacadeLocal {

    @PersistenceContext(unitName = "PruebaDiagnosticaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ModoPagoFacade() {
        super(ModoPago.class);
    }
    
}
