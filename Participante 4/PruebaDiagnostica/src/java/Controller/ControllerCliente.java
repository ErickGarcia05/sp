/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import EJB.ClienteFacadeLocal;
import Entity.Cliente;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author christian.ramirezusa
 */
@ManagedBean
@SessionScoped
public class ControllerCliente implements Serializable{
    
    @EJB
    private ClienteFacadeLocal clienteEJB;
    private List<Cliente> listaClientes;
    private Cliente cliente;

    private String mensaje;
    
    public List<Cliente> getListaClientes() {
        return listaClientes = clienteEJB.findAll();
    }

    public void setListaClientes(List<Cliente> listaClientes) {
        this.listaClientes = listaClientes;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    @PostConstruct
    public void init(){
        this.cliente = new Cliente();
    }
    
    public void limpiar(){
        this.cliente = new Cliente();
    }
    
    public void cargarId(Cliente c){
        this.cliente = c;
    }
    
        
    public void guardar(){
        try {
            this.clienteEJB.create(cliente);
            this.mensaje = "Cliente insertado";
        } catch (Exception e) {
            this.mensaje = "Error al insertar el cliente";
        }
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void editar(){
        try {
            this.clienteEJB.edit(cliente);
            this.mensaje = "Cliente editado correctamente";
        } catch (Exception e) {
            this.mensaje = "Error al editar el cliente";
        }
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void eliminar(Cliente c){
        try {
            this.clienteEJB.remove(c);
            this.mensaje = "Cliente eliminado correctamente";
        } catch (Exception e) {
            this.mensaje = "Error al eliminar el cliente";
        }
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public String formatoFecha(Date d){
        try {
            SimpleDateFormat ffecha = new SimpleDateFormat("dd-MM-yyyy");
            String fecha = ffecha.format(d);
            return fecha;
        } catch (Exception e) {
            return "Error al convertir";
        }
    }
    
    public void sp(){
        try {
            this.clienteEJB.call("JAVA001", "2019-01-2", 1, 1);
            
        } catch (Exception e) {
        }
    }
}
