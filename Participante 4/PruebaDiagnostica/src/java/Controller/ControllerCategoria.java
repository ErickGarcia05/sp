/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import EJB.CategoriaFacadeLocal;
import Entity.Categoria;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author christian.ramirezusa
 */
@ManagedBean
@SessionScoped
public class ControllerCategoria implements Serializable{
    
    @EJB
    private CategoriaFacadeLocal categoriaEJB;
    private List<Categoria> listaCategorias;
    private Categoria categoria;
    
    private String mensaje;

    public List<Categoria> getListaCategorias() {
        return listaCategorias = categoriaEJB.findAll();
    }

    public void setListaCategorias(List<Categoria> listaCategorias) {
        this.listaCategorias = listaCategorias;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
    
    @PostConstruct
    public void init(){
        this.categoria = new Categoria();
    }
    
    public void limpiar(){
        this.categoria = new Categoria();
    }
    
    public void cargarId(Categoria c){
        this.categoria = c;
    }
    
    public void guardar(){
        try {
            this.categoriaEJB.create(categoria);
            this.mensaje = "Categoria Insertada correctamente";
        } catch (Exception e) {
            this.mensaje = "Error al insertar categoria" + e.getMessage();            
        }
        
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);        
    }
    
    public void editar(){
        try {
            this.categoriaEJB.edit(categoria);
            this.mensaje = "Categoria editada correctamente";
        } catch (Exception e) {
            this.mensaje = "Error al editar categoria" + e.getMessage();            
        }
        
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);        
    }
    
    public void eliminar(Categoria c){
        try {
            this.categoriaEJB.remove(c);
            this.mensaje = "Categoria eliminada correctamente";
        } catch (Exception e) {
            this.mensaje = "Error al eliminar una categoria" + e.getMessage();            
        }
        
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);        
    }
    
    
}
