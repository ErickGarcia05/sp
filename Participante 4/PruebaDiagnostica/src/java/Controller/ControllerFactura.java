/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import EJB.FacturaFacadeLocal;
import Entity.Cliente;
import Entity.Factura;
import Entity.ModoPago;
import java.io.Serializable;
import static java.util.EnumSet.range;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author christian.ramirezusa
 */
@ManagedBean
@SessionScoped
public class ControllerFactura implements Serializable {

    @EJB
    private FacturaFacadeLocal facturaEJB;
    private List<Factura> listaFacturas;
    private Factura factura;
    private Cliente cliente;
    private ModoPago modoPago;

    private String mensaje;

    public List<Factura> getListaFacturas() {
        return listaFacturas = facturaEJB.findAll();
    }

    public void setListaFacturas(List<Factura> listaFacturas) {
        this.listaFacturas = listaFacturas;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ModoPago getModoPago() {
        return modoPago;
    }

    public void setModoPago(ModoPago modoPago) {
        this.modoPago = modoPago;
    }

    @PostConstruct
    public void init() {
        this.cliente = new Cliente();
        this.modoPago = new ModoPago();
        this.factura = new Factura();
        //int ultimo = Integer.parseInt(this.facturaEJB.ultimoRegistro().getNum_factura());
        //ultimo = ultimo + 1;
        //System.out.println("Factura" +facturas);
        //this.factura.setNum_factura(this.facturaEJB.ultimoRegistro().getNum_factura());
        
        //int ultimaFactura = this.facturaEJB.findRange(maxFactura range));
        
    }

    public void limpiar() {
        this.cliente = new Cliente();
        this.modoPago = new ModoPago();
        this.factura = new Factura();
    }

    public void guardar() {
        
        try {
            int ultima = facturaEJB.ultimoRegistro().getId_factura();
            //String num_factura = this.factura.getNum_factura();
            //int num_Factura2 = Integer.parseInt(this.factura.getNum_factura());
            System.out.println("Ultimo: " +ultima);
            if (ultima < 10) {
                this.factura.setNum_factura("JAVA000" + (ultima+1));
            } else if (ultima < 100) {
                this.factura.setNum_factura("JAVA00" + (ultima+1));
            } else if (ultima < 1000) {
                this.factura.setNum_factura("JAVA0" + (ultima+1));
            } else if (ultima < 9999) {
                this.factura.setNum_factura("JAVA" + (ultima+1));
            } else {                
                this.factura.setNum_factura("JAVA0001");
                this.factura.setNum_factura("Error al seguir formato");
            }

            this.factura.setNum_factura(this.factura.getNum_factura());
            //this.factura.setNum_factura(num_factura);
            this.factura.setId_factura(0);
            this.factura.setId_cliente(cliente);
            this.factura.setNum_pago(modoPago);
            //this.facturaEJB.call(factura);
            this.facturaEJB.call2(factura);
            this.mensaje = "La factura se inserto Correctamente";
        } catch (Exception e) {
            
            System.out.println("Error en controller: " +e.getMessage());
            this.mensaje = "Error al insertar la factura ";
        }
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void editar() {
        try {            
            this.factura.setId_cliente(cliente);
            this.factura.setNum_pago(modoPago);
            this.facturaEJB.edit(factura);
            this.mensaje = "La factura se edito Correctamente";
        } catch (Exception e) {
            this.mensaje = "Error al editar la factura";
        }
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void eliminar(Factura f) {
        try {                        
            this.facturaEJB.remove(f);
            this.mensaje = "La factura se elimino Correctamente";
        } catch (Exception e) {
            this.mensaje = "Error al eliminar la factura";
        }
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
